const NAME = 'utils'

function toTextGroup (node, f) {
  return node.children.map(c => toText(c, f)).join('')
}

function toText (node, f) {
  switch (node.type) {
    case 'text':
      return node.value.replace(/\s+/g, ' ')
    case 'code':
      return node.value
    case 'emphasis':
    case 'paragraph':
    case 'span':
      return toTextGroup(node, f)
  }
  if (node.children) return toTextGroup(node, f)
  if (f !== null) {
    f.message(`unknown text type: ${node.type}`, node.position,
      `${NAME}:no-unknown-text-type`)
  }
  return ''
}

function pushPos (pos, text) {
  const e = pos.end
  const hasNewline = text.includes('\n')
  return {
    start: {
      line: e.line,
      column: e.column + 1,
      offset: e.offset + 1
    },
    end: {
      line: hasNewline
        ? e.line + text.match(/\n/g).length
        : e.line,
      column: hasNewline
        ? text.match(/\n.*?$/).length
        : e.column + text.length,
      offset: e.offset + 1 + text.length
    }
  }
}

function splitText (text, re, position) {
  const nodes = []
  let p = 0
  let lastpos = {
    end: {
      line: position.start.line,
      column: position.start.column - 1,
      offset: position.start.offset - 1
    }
  }
  for (const m of text.matchAll(re)) {
    const prevText = text.substring(p, m.index)
    lastpos = pushPos(lastpos, prevText)
    nodes.push({
      match: null,
      text: prevText,
      position: lastpos
    })
    lastpos = pushPos(lastpos, m[0])
    nodes.push({
      match: m,
      text: m[0],
      position: lastpos
    })
    p = m.index + m[0].length
  }
  const lastText = text.substring(p, text.length)
  lastpos = pushPos(lastpos, lastText)
  nodes.push({
    match: null,
    text: lastText,
    position: lastpos
  })
  return nodes.filter(n => n.text)
}

function isLabelNode (node) {
  return node.data && node.data.directiveLabel
}

function splitContainer (node, f) {
  const cNodes = node.children.filter(c => !isLabelNode(c))
  const lNodes = node.children.filter(c => isLabelNode(c))
  if (!lNodes.length) {
    return [null, cNodes]
  }
  if (lNodes.length > 1) {
    f.message(`multiple label nodes found for directive "${node.type}"`,
      node.position, `${NAME}:has-one-label`)
  }
  return [lNodes[0], cNodes]
}

export {
  splitContainer,
  splitText,
  toText
}
