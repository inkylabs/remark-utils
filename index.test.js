/* eslint-env jest */
import { toText } from '.'

describe('toText', () => {
  [
    [{
      type: 'text',
      value: 'blah\nblah'
    },
    'blah blah']
  ]
    .forEach(([i, e]) => {
      it(e, () => {
        expect(toText(i)).toEqual(e)
      })
    })
})
